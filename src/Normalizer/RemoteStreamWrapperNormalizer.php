<?php

namespace Drupal\remote_stream_wrapper_rest\Normalizer;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\serialization\EntityResolver\EntityResolverInterface;
use Drupal\serialization\Normalizer\EntityReferenceFieldItemNormalizer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Embeds Remote Stream Wrapper item into entity.
 */
class RemoteStreamWrapperNormalizer extends EntityReferenceFieldItemNormalizer {

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = 'Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem';

  /**
   * The entity resolver.
   *
   * @var \Drupal\serialization\EntityResolver\EntityResolverInterface
   */
  protected $entityResolver;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_resolver.manager'),
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityResolverInterface $entity_Resolver, EntityTypeManager $entity_type_manager, AccountProxyInterface $current_user) {
    $this->entityResolver = $entity_Resolver;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {

    if (!isset($context['target_instance'])) {
      throw new InvalidArgumentException('$context[\'target_instance\'] must be set to denormalize with the FieldItemNormalizer');
    }

    if ($context['target_instance']->getParent() == NULL) {
      throw new InvalidArgumentException('The field item passed in via $context[\'target_instance\'] must have a parent set.');
    }

    if (array_key_exists('uri', $data)) {

      $fileStorage = $this->entityTypeManager->getStorage('file');

      $file = $fileStorage->create([
        'uri' => $data['uri'],
        'uid' => $this->currentUser->id(),
      ]);

      $file->save();

      // Reassign target_id, keeping everything else.
      $data['target_id'] = $file->id();

    }

    parent::denormalize($data, $class, $format, $context);

  }

}
